var scroll_top = document.getElementById('scroll-top')

// hides the scroll_top arrow when near the top of the page
var scroll_top_pop_up = new Waypoint({
  element: document.getElementById('view-top'),
  handler: function(direction) {
    if (direction == "down") {
      scroll_top.classList.remove("disabled");
      scroll_top.classList.toggle("show");
    } else {
      scroll_top.classList.add("disabled");
      scroll_top.classList.toggle("show");
    }
  },
  offset: '-10%'
})


// when at the bottom of the page, pushes the scroll top arrow slightly up so it isn't on the footer
var scroll_top_sticky = new Waypoint({
  element: document.getElementById('view-bottom'),
  handler: function(direction) {
    if (direction == "down") {
      scroll_top.style["bottom"] = "70px";
    } else {
      scroll_top.style["bottom"] = "5px";
    }
  },
  offset: 'bottom-in-view'
})
