
// based on Bootstrap script for custom validation
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // fetch submit button
    var button = document.getElementById('submit-button');

    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {

      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);

      // check form validity after each change, so we can trigger the submit button aspect change when the form is ready to be sent
      form.addEventListener('change', function() {
        if (form.checkValidity() === true) {
          enableButton();
        } else {
          disableButton();
        }
      });

      // resets the submit button aspect when the reset button is clicked
      form.addEventListener('reset', function() {
        disableButton();
      });

    });

    // button enable/disable utilities
    function enableButton() {
      button.classList.remove("btn-light");
      button.classList.add("btn-dark");
      button.setAttribute("data-target", "#submit-modal");
    }

    function disableButton() {
      button.classList.add("btn-light");
      button.classList.remove("btn-dark");
      button.removeAttribute("data-target");
    }

  }, false);
})();
