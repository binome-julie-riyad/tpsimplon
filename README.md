# FORMATION SIMPLON
## TP3 - Site Simplon Grenoble

 * TP numéro 3 formation dév web et web mobile.
 * 2 pages réalisées:
									-index.html - L'accueil contenant une présentation de la fabrique Grenoble et de son équipe, ainsi que le contenue des deux formations.
                  -contact.html - Contactez-nous, la page contenant les coordonnées géographiques ainsi qu'un formulaire de contact.
                  -accueil.scss - Fichier SCSS spécifique a la page index.html
                  -contact.scss - Fichier SCSS spécifique a la page contact.html
                  -style.scss - Fichier SCSS global du TP contenant les classes partagé entre les deux pages.
### Groupe :
* Riyad Bakour
* Julie Souchet

### Objectif : 
Créer un mini site pour le centre de formation Simplon de la ville de la promo. Le site devra présenter la formation Simplon ainsi que donner les informations utiles sur la fabrique de Simplon.
Vous devez respecter la charte graphique du groupe Simplon (couleur, police d'écriture, etc ...). Vous devez aussi intégrer le logo du Simplon.

Aucune maquette graphique n'a été définit pour le site, vous avez carte blanche.

Le site devra être réalisé en HTML et CSS.
* Le site doit être responsive
* Vous devez utiliser SASS pour la création de votre CSS
* Vous devez utiliser une convention de nommage
* Le site doit contenir un formulaire de contact, non fonctionnel mais les champs sont validés :
  * Nom
  * Prenom
	* Genre (Mr / Mme / Autre)
	* Email
	* Message (max 300 caractères)
* Les images doivent être optimisées pour le web (taille adaptée / poids etc)
* Les champs dédiés au référencement doivent être complétés (images et meta)
* Vous pouvez utiliser un framework CSS : Bootstrap ou autre
* Vous pouvez utiliser toutes les librairies CSS ou JS que vous voulez

### Retour
Dépôt présent sur [GitLab](https://gitlab.com/binome-julie-riyad/tpsimplon).

À rendre pour le 03 juillet 2020 au soir.
x) ah bah oui ca me parait niquel